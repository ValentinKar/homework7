<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки

class certificate_GD 
{
	public function generate($name, $theme, $rating)
	{   
		$im = imagecreatetruecolor(1100, 820);   // создаю полноцветную картинку 1100 на 820 пикселей

		$color_back = imagecolorallocate($im, 255, 255, 255); // создаю цвет в формате RGB для фона
		$color_sert = imagecolorallocate($im, 51, 0, 153);   //  создаю цвет в формате RGB для текста
		$color_data = imagecolorallocate($im, 0, 51, 0);    //  создаю цвет в формате RGB для фона
		$color_sign = imagecolorallocate($im, 255, 0, 0);
		
		$font_sert = '../fonts/Aardvark.ttf';          // подключаю фойл для шрифта
		$font_data = '../fonts/a_BodoniNovaNr.ttf';   // подключаю фойл для шрифта
		$font_sign = '../fonts/DS-SonOf.ttf'; 

		$im_box = imagecreatefrompng('../img/fon.png');  // читаю файл с картинкой с диска

		imagefill($im, 0, 0, $color_back);         // заполняю изначально созданную картинку фоновым цветом начиная с координаты 0, 0

		imagecopy($im, $im_box, 185, 50, 0, 0, 730, 494);    //копирую на созданную картинку картинку из файла

		imagettftext($im, 70, 0, 180, 510, $color_sert, $font_sert, 'СЕРТИФИКАТ');         // пишу текст на созданную картинку
		imagettftext($im, 20, 0, 400, 570, $color_sert, $font_sert, 'удостоверяет, что');  // 
		imagettftext($im, 30, 0, 180, 650, $color_data, $font_data,  $name.' выполнил тест');
		imagettftext($im, 30, 0, 180, 700, $color_data, $font_data,  'Тема теста: '.$theme);
		imagettftext($im, 30, 0, 180, 750, $color_data, $font_data,  'Оценка: '.$rating);
		imagettftext($im, 40, 50, 800, 770, $color_sign, $font_sign, 'ПЕЧАТЬ');
		// header('Content-Type: image/png'); 
		header('Content-Disposition: attachment; filename="certificate.png"');     // 
		imagepng($im);             // вывожу png изображение в файл
		imagedestroy($im);          // 
	}
	
}