<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
include_once 'function.php'; 
$content = file_get_contents("tests.json");    //загружаю json-данные из файла
$tests = json_decode($content, true);     //json-данные записываю в массив

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>List</title>
</head>
<body>

<?php if (isset($_GET['rating']) )  { ?>
Оценка за выполненный тест: <?php  echo $_GET['rating']; }  ?>
<br/>

<?php if (isRating() )  { ?>
<img src='<?php   // картинка с сертификатом
echo "module_img/certificate.php?name=".$_GET['name']."&theme=".$_GET['theme']."&rating=".$_GET['rating']; 
?>' width="220" height="164" border="1px" alt="рамка для сертификата, получаемого по результатам прохождения теста" title="что-бы скачать, кликни правой кнопкой и нажми сохранить картинку как"> <?php };  ?>

<h1>Список тестов: </h1>

<ol>
	<?php 
	$i = 1;
	foreach ($tests as $key => $array)  
	{ 
	?>
		<a href="test.php?number=<?php echo $i; ?>">
		<li><?php  echo $key; ?></li>
		</a>
	<?php  
	$i++;
	}; 
	?>
</ol>

</body>
</html>